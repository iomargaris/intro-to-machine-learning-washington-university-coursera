# About course

This first course treats the machine learning method as a black box.  Using this abstraction, I focus on understanding tasks of interest, matching these tasks to machine learning tools, and assessing the quality of the output. Together, these pieces form the machine learning pipeline, which I use in developing intelligent applications.

At the end of the first course I have studied how to predict house prices based on house-level features, analyze sentiment from user reviews, retrieve documents of interest, recommend products, and search for images, through hands-on practice with use cases

## Syllabus:
https://www.coursera.org/learn/ml-foundations

**Week 1: Welcome**

* Refresher on Python
* Intro to Jupyter Notebooks
* Introduction to TuriCreate (former GraphLab Create)

**Week 2: Regression - Predicting house prices**

* Liner Regression
* Loading and exploring the house sale data
* Splitting the data into training and test sets2m
* Learning a simple regression model to predict house prices from house size
* Evaluating error (RMSE) of the simple model
* Visualizing predictions of simple model with Matplotlib
* Inspecting the model coefficients learned
* Learning a model to predict house prices


**Week 3: Classification - Analyzing Sentiment**

* Examples of classification tasks
* Linear classifiers
* Decision boundaries
* False positives, false negatives, and confusion matrices6m
* Learning curves
* Loading & exploring product review data2m
* Creating the word count vector
* Exploring the most popular product
* Defining which reviews have positive or negative sentiment
* Training a sentiment classifier
* Evaluating a classifier & the ROC curve
* Applying model to find most positive & negative reviews for a product
* Exploring the most positive & negative aspects of a product


**Week 4: Clustering and Similarity - Retrieving Documents**

* What is the document retrieval task?
* Word count representation for measuring similarity
* Prioritizing important words with tf-idf
* Calculating tf-idf vectors
* Retrieving similar documents using nearest neighbor search
* Clustering documents: An unsupervised learning task
* k-means: A clustering algorithm
* Loading & exploring Wikipedia data
* Exploring word counts
* Computing & exploring TF-IDFs
* Computing distances between Wikipedia articles
* Building & exploring a nearest neighbors model for Wikipedia articles


**Week 5: Recommending Products**

* Recommender systems overview
* Collaborative filtering: People who bought this also bought
* A performance metric for recommender systems
* Optimal recommenders
* Precision-recall curves
* Loading and exploring song data
* Creating & evaluating a popularity-based song recommender
* Creating & evaluating a personalized song recommender
* Using precision-recall to compare recommender models


**Week 6: Intro to Deep Learning - Searching for Images**

* What is a visual product recommender?
* Learning very non-linear features with neural networks
* Application of deep learning to computer vision
* Deep learning performance
* Deep Features
* Loading image data
* Training & evaluating a classifier using raw image pixels
* Training & evaluating a classifier using deep features
* Loading image data
* Creating a nearest neighbors model for image retrieval
* Querying the nearest neighbors model to retrieve images
* Querying for the most similar images for car image
* Displaying other example image retrievals with a Python lambda

**Learning Outcomes:**

* Identify potential applications of machine learning in practice.  
* Describe the core differences in analyses enabled by regression, classification, and clustering.
* Select the appropriate machine learning task for a potential application.  
* Apply regression, classification, clustering, retrieval, recommender systems, and deep learning.
* Represent your data as features to serve as input to machine learning models. 
* Assess the model quality in terms of relevant error metrics for each task.
* Utilize a dataset to fit a model to analyze new data.
* Build an end-to-end application that uses machine learning at its core.  
* Implement these techniques in Python.