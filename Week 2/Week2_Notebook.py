#!/usr/bin/env python
# coding: utf-8

# # Import turi create

# In[94]:


import turicreate as tc


# # Load the sales data to an SFrame

# In[95]:


sales = tc.SFrame('/Users/iomargaris/Desktop/Machine Learning Specialization -  Coursera/Week 2/home_data.gl/')


# In[96]:


sales.column_names


# # Visualize the data

# In[97]:


tc.visualization.scatter(sales['sqft_living'],sales['price'],xlabel='Sqrft_Living', ylabel='Price')


# # Create a simple regression model of sqft_living and price

# - Split sales SFrame into training ans test sets
# - Seed=0 to always split the SFrame the same way

# In[98]:


training_set,test_set = sales.random_split(.8,seed=0)


# ## Build the regression model

# In[99]:


sqft_model = tc.linear_regression.create(training_set, target='price', features=['sqft_living'], validation_set=None)


# # Evaluate the simple regression model

# In[100]:


print test_set['price'].mean()


# In[101]:


print sqft_model.evaluate(training_set)


# ## Lets show what our predictions look like

# In[102]:


import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')


# In[103]:


plt.plot(test_set['sqft_living'], test_set['price'],'.',
        test_set['sqft_living'],sqft_model.predict(test_set),'-')


# ## Extracting the coefficients from the model

# In[104]:


print sqft_model.coefficients


# # Exploring other features in the data

# In[105]:


my_features = ['bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'zipcode']


# In[106]:


sales[my_features].show()


# In[107]:


tc.visualization.box_plot(sales['zipcode'], sales['price'], xlabel='Zipcode', ylabel='Price')


# # Building a regression model with more features

# In[108]:


my_features_model = tc.linear_regression.create(training_set, target='price', features=my_features, validation_set=None)


# In[109]:


print my_features


# In[110]:


print sqft_model.evaluate(test_set)
print my_features_model.evaluate(test_set)


# # Apply these learned models to predict prices of 3 houses

# In[111]:


house1 = sales[sales['id']=='5309101200']


# In[112]:


house1


# <img src="http://info.kingcounty.gov/Assessor/eRealProperty/MediaHandler.aspx?Media=2916871">

# In[113]:


print house1['price']


# In[114]:


print sqft_model.predict(house1)


# In[115]:


print my_features_model.predict(house1)


# In this case, the model with more features provides a worse prediction than the simpler model with only 1 feature.  However, on average, the model with more features is better.

# # Prediction for a second, fancier house

# In[116]:


house2 = sales[sales['id']=='1925069082']


# In[117]:


house2


# <img src="https://ssl.cdn-redfin.com/photo/1/bigphoto/302/734302_0.jpg">

# In[118]:


print sqft_model.predict(house2)


# In[119]:


print my_features_model.predict(house2)


# ## The last, even fancier house (Bill Gates)

# In[120]:


bill_gates = {'bedrooms':[8], 
              'bathrooms':[25], 
              'sqft_living':[50000], 
              'sqft_lot':[225000],
              'floors':[4], 
              'zipcode':['98039'], 
              'condition':[10], 
              'grade':[10],
              'waterfront':[1],
              'view':[4],
              'sqft_above':[37500],
              'sqft_basement':[12500],
              'yr_built':[1994],
              'yr_renovated':[2010],
              'lat':[47.627606],
              'long':[-122.242054],
              'sqft_living15':[5000],
              'sqft_lot15':[40000]}


# <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Bill_gates%27_house.jpg/2560px-Bill_gates%27_house.jpg">

# In[121]:


bill_gates_sf = tc.SFrame(bill_gates)


# In[122]:


print sqft_model.predict(bill_gates_sf)


# In[123]:


print my_features_model.predict(bill_gates_sf)


# The model predicts a price of over $13M for this house! But we expect the house to cost much more. (There are very few samples in the dataset of houses that are this fancy, so we don't expect the model to capture a perfect prediction here.)

# # Programming Assignments

# ## Assignment 1: Selection and summary statistics:
# In the notebook we covered in the module, we discovered which neighborhood (zip code) of Seattle had the highest average house sale price. Now, take the sales data, select only the houses with this zip code, and compute the average price. Save this result to answer the quiz at the end.

# In[124]:


zip_code_98039 = sales[sales['zipcode']=='98039']


# In[125]:


zip_code_98039


# Getting the average price of the houses with zipcode equal to 98039

# In[126]:


zip_code_98039['price'].mean()


# ## Assignment 2: Filtering data
# One of the key features we used in our model was the number of square feet of living space (‘sqft_living’) in the house. For this part, we are going to use the idea of filtering (selecting) data. In particular, we are going to use logical filters to select rows of an SFrame. You can find more info in the Logical Filter section of this documentation. Using such filters, first select the houses that have ‘sqft_living’ higher than 2000 sqft but no larger than 4000 sqft.What fraction of the all houses have ‘sqft_living’ in this range? Save this result to answer the quiz at the end.

# -  ‘sqft_living’ higher than 2000 sqft but no larger than 4000 sqft.

# In[127]:


houses_filtered_2000_4000 = sales[(sales['sqft_living'] > 2000) & (sales['sqft_living'] <= 4000)]


# In[128]:


houses_filtered_2000_4000.num_rows


# In[129]:


houses_filtered_2000_4000.materialize()


# In[130]:


houses_filtered_2000_4000.num_rows


# Filtered houses from 2000 up to 4000 contain: 9118 rows x 21 columns

# In[131]:


sales.materialize()


# In[132]:


sales.num_rows


# Total sales SF contains: 21613 rows x 21 columns

# In[133]:


#21613 100
#9118 x

x = (9118*100)/.21613
x


# So, the fraction of the all houses that have ‘sqft_living’ in this range is 4218757.229445241%

# ## Assignment 3: Building a regression model with several more features
# What is the difference in RMSE between the model trained with my_features and the one trained with advanced_features? Save this result to answer the quiz at the end.

# In[135]:


advanced_features = [
'bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'zipcode',
'condition', # condition of house				
'grade', # measure of quality of construction				
'waterfront', # waterfront property				
'view', # type of view				
'sqft_above', # square feet above ground				
'sqft_basement', # square feet in basement				
'yr_built', # the year built				
'yr_renovated', # the year renovated				
'lat', 'long', # the lat-long of the parcel				
'sqft_living15', # average sq.ft. of 15 nearest neighbors 				
'sqft_lot15', # average lot size of 15 nearest neighbors 
]


# ### First lets build a linear regression model based on our 'advanced_features'

# In[136]:


advanced_features_model = tc.linear_regression.create(training_set, target='price', features=advanced_features, validation_set=None)


# ## Comapring evaluations of all models:

# In[140]:


sqft_model.evaluate(test_set)


# In[138]:


my_features_model.evaluate(test_set)


# In[139]:


advanced_features_model.evaluate(test_set)


# In[143]:


rmse_advanced_features = 156831.11680192588
rmse_my_features = 179542.43331269105
rmse_diff = rmse_my_features - rmse_advanced_features
rmse_diff


# In[ ]:




