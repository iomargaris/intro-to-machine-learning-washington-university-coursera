
# coding: utf-8

# # Imports

# In[66]:

import turicreate as tc
from turicreate import SFrame


# In[67]:

print ('Hello World!')


# # This is some text
# 
# * This is a list item
# * This is a list item
# 

# In[68]:

i = 1


# In[69]:

type(i)


# In[70]:

f = 4.1


# In[71]:

type(f)


# In[72]:

b = True


# In[73]:

type(b)


# In[74]:

s = 'This is a string'


# In[75]:

type(s)


# ## Advanced Python types

# In[76]:

l = [1,3,2]


# In[77]:

print(l)


# In[78]:

type(l)


# In[79]:

d = {'foo': 1, 'bar': 2.8, 's': 'This is a dictionary!'}


# In[80]:

print(d)


# In[81]:

type(d)


# In[82]:

print (d['s'])


# In[83]:

n = None


# In[84]:

type(n)


# ## Advanced Printing

# In[85]:

print('Our float value is %s. Our int value is %s.' % (f,i))


# In[86]:

square = lambda x: x*x


# In[87]:

square(3)


# In[88]:

sf = SFrame(data='/Users/iomargaris/Desktop/Machine Learning Specialization -  Coursera/Week 1/people-example.csv')


# In[89]:

sf


# In[90]:

sf.tail()


# In[91]:

sf.show()


# In[92]:

sf['age'].show('categorical')


# In[93]:

sf['age']


# In[94]:

sf['Country']


# In[95]:

sf['age'].max()


# In[96]:

sf['age'].mean()


# In[97]:

sf


# In[98]:

sf['Full Name'] = sf['First Name'] + ' ' + sf['Last Name']


# In[99]:

sf


# In[100]:

sf['age'] * sf['age']


# In[101]:

sf


# In[102]:

sf['Country']


# In[103]:

sf['Country'].show()


# In[108]:

def transform_country(country):
    if country == 'USA':
        return 'United States'
    else:
        return country


# In[109]:

transform_country('Brazil')


# In[110]:

sf['Country']


# In[111]:

transform_country('USA')


# In[113]:

sf['Country'].apply(transform_country)


# In[114]:

sf['Country'].show()


# In[115]:

sf['Country'] = sf['Country'].apply(transform_country)


# In[117]:

sf['Country'].show()


# In[118]:

sf


# In[119]:

sf.show()


# In[ ]:



